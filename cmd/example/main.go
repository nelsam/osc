package main

import (
	"time"

	"gitlab.com/taotetek/osc"
)

func main() {
	c , err := osc.NewClient("192.168.1.111", 8000)
	if err != nil {
		panic("oops")
	}
	defer c.Close()

	for i := 0; i < 100; i++ {
		c.SetControlVoltageA(0.0)
		c.SetControlVoltageB(1.0)
		c.SetTriggerA(1)
		c.SetTriggerB(0)

		time.Sleep(1000 * time.Millisecond)
		
		c.SetControlVoltageA(1.0)
		c.SetControlVoltageB(0.0)
		c.SetTriggerA(0)
		c.SetTriggerB(1)

		time.Sleep(1000 * time.Millisecond)
	}
}
