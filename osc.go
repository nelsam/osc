package osc

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net"
)

type oscControlVoltageAddress string
type oscTriggerAddress string

const (
	triggerA        oscTriggerAddress = "/osm/a/tr"
	triggerB        oscTriggerAddress = "/osm/b/tr"
	
	controlVoltageA oscControlVoltageAddress = "/osm/a/cv"
	controlVoltageB oscControlVoltageAddress = "/osm/b/cv"

	typeTagInt32   = "i"
	typeTagFloat32 = "f"
	typeTagString  = "s"
	typeTagBlob    = "b"

	typeTagStart = ","
)

func writePaddedString(buf *bytes.Buffer, data string) error {
	n, err := buf.WriteString(data)
	if err != nil {
		return err
	}
	numPadBytes := 4*(n/4+1) - n
	if numPadBytes > 0 {
		pad := make([]byte, numPadBytes)
		_, err := buf.Write(pad)
		if err != nil {
			return err
		}
	}
	return nil
}

type Client struct {
	conn net.Conn
}

func NewClient(host string, port int) (*Client, error) {
	conn, err := net.Dial("udp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		return nil, err
	}
	return &Client{conn: conn}, nil
}

func (c *Client) setControlVoltage(addr oscControlVoltageAddress, data float32) error {
	var buf bytes.Buffer
	err := writePaddedString(&buf, string(addr))
	if err != nil {
		return err
	}

	err = writePaddedString(&buf, fmt.Sprintf("%s%s", typeTagStart, typeTagFloat32))
	if err != nil {
		return err
	}

	err = binary.Write(&buf, binary.BigEndian, data)
	if err != nil {
		return err
	}

	_, err = c.conn.Write(buf.Bytes())
	return err
}

func (c *Client) SetControlVoltageA(data float32) error {
	err := c.setControlVoltage(controlVoltageA, data)
	return err
}

func (c *Client) SetControlVoltageB(data float32) error {
	err := c.setControlVoltage(controlVoltageB, data)
	return err
}

func (c *Client) setTrigger(addr oscTriggerAddress, data int32) error {
	var buf bytes.Buffer
	err := writePaddedString(&buf, string(addr))
	if err != nil {
		return err
	}

	err = writePaddedString(&buf, fmt.Sprintf("%s%s", typeTagStart, typeTagInt32))
	if err != nil {
		return err
	}

	err = binary.Write(&buf, binary.BigEndian, data)
	if err != nil {
		return err
	}

	_, err = c.conn.Write(buf.Bytes())
	return err
}

func (c *Client) SetTriggerA(data int32) error {
	err := c.setTrigger(triggerA, data)
	return err
}

func (c *Client) SetTriggerB(data int32) error {
	err := c.setTrigger(triggerB, data)
	return err
}

func (c *Client) Close() {
}
